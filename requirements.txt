pip==23.2.1
asgiref==3.5.2
cryptography==37.0.1
Django==4.1
numpy==1.23.3
packaging==21.3
Pillow==9.2.0
gunicorn==20.1.0
psycopg2==2.9.3
pyOpenSSL==22.0.0
pyparsing==3.0.9
PySocks==1.7.1
python-dotenv==1.0.0
pytz==2022.2.1
setuptools==63.4.1
sqlparse==0.4.2
typing_extensions==4.7.1
wheel==0.37.1
